import pymysql

class DBUpdater:
    def __init__(self):
        """생성자 : MariaDB 연결 및 종목코드 딕셔너리 생성"""
        self.conn = pymysql.connect(host='localhost', user='winner', password='winner',
                                    db='winner', charset='utf8')
        with self.conn.cursor() as curs:
            sql = """
            CREATE TABLE IF NOT EXISTS company_info(
                code varchar(20),
                company varchar(40),
                last_update DATE,
                primary key (code)
                )
            """

            curs.execute(sql)
            #todo 테이블 양 추가하면서 코딩짜기 226페이지 참조
            sql = """
            CREATE TABLE IF NOT EXISTS daily_price(
                code varchar(20),
                date DATE,
                open BIGINT(20),
                high BIGINT(20)
                low BIGINT(20),
                close BIGINT(20),
                diff BIGINT(20),
                
                )
            """

    def __del__(self):
        """소멸자: MariaDB 연결 해제"""

    def read_krx_code(self):
        """KRX로부터 상장법인목록 파일을 읽어와서 데이터프레임으로 반환"""

    def update_comp_info(self):
        """네이버 금융에서 주식 시세를 읽어서 데이터프레임으로 반환"""

    def read_naver(selfs, code, company, pages_to_fetch):
        """네이버 금융에서 주식 시세를 일겅서 데이터프레임으로 반환"""

    def replace_into_db(self, db, num, code, company):
        """네이버 금융에서 읽어온 주식 시세를 DB에 Replace"""

    def update_daily_price(self, pages_to_fetch):
        """KRX 상장법인의 주식 시세를 네이버로부터 읽어서 DB에 업데이트"""

    def execute_daily(self):
        """실행 즉시 및 매일 오후 다섯시에 daily_price 테이블 업데이트"""

if __name__ == '__main__':
    dbu = DBUpdater()
    dbu.execute_daily(0)